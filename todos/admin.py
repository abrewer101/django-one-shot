from .models import TodoList, TodoItem
from django.contrib import admin

# Register your models here.
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'created_on')

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'list', 'due_date', 'is_completed')
    list_filter = ('list',)
    search_fields = ('task',)
